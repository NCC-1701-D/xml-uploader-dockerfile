FROM alpine/git as repository
WORKDIR /app
RUN git clone https://zeisekku@bitbucket.org/NCC-1701-D/xml-uploader.git

FROM composer as builder
COPY --from=repository /app/xml-uploader/ /app
WORKDIR /app
COPY . ./
RUN composer install

FROM php:8-cli-alpine
COPY --from=builder /app /var/www/html
COPY credentials.json /var/www/html
WORKDIR /var/www/html
RUN cp .env.dist .env
RUN rm .env.dist

ENTRYPOINT ["php", "src/Application.php"]
