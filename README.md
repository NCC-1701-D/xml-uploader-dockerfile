# Xml Uploader

## Prerequisites
1. [Docker](https://docs.docker.com/engine/install/)
2. Project at Google Cloud Platform with:
    - [enabled Google Sheets API and Google Drive API](https://developers.google.com/workspace/guides/create-project)
    - [service account and downloaded JSON key](https://cloud.google.com/docs/authentication/getting-started)

## Setting up
1. Before you build the application, you need to copy the downloaded **JSON key** to the directory with the **Dockerfile**. 
The copied file must be named **credentials.json**. This directory contains a sample **credentials.json.dist** file.

2. Building image:

        docker build -t xml-uploader .

## How to use

### Uploading xml file from an external source:
You will use the command:

    docker run -it --network="host" xml-uploader xml-to-spreadsheet:upload

with arguments:

    file-path             Path to the file
    owner-email           Document owner e-mail

which gives you:

    docker run -it --network="host" xml-uploader xml-to-spreadsheet:upload ftp://username:password@some.site.io/file.xml owner.email@gmail.com

of course, instead of **owner.email@gmail.com** you must use the e-mail address of Google Sheets user, who will be the owner of the uploaded document.

#### Known issues:
Sometimes you have to try few times because they can be some connection issues.

### Uploading xml file from a local source:

#### If you want to upload a file from the current directory, you will use the command:

    docker run -it -v $(pwd):/var/www/html/data xml-uploader xml-to-spreadsheet:upload /var/www/html/data/file.xml owner.email@gmail.com

where:

    $(pwd) - this returns the absolute path to the current directory with the file we want to upload
    /var/www/html/data - directory in container where current directory will be mounted
    /var/www/html/data/file.xml - an absolute path to file in the conatiner(the application is in /var/www/html directory)

You can also use a relative path to the file in the container instead of an absolute path to the file in the container:

    docker run -it -v $(pwd):/var/www/html/data xml-uploader xml-to-spreadsheet:upload data/file.xml owner.email@gmail.com

instead of

    docker run -it -v $(pwd):/var/www/html/data xml-uploader xml-to-spreadsheet:upload /var/www/html/data/file.xml owner.email@gmail.com

#### If you want to upload a file from a directory other than the current one, you will use the command:
 
    docker run -it -v /absolute/path/to/the/directory/with/file/to/upload:/var/www/html/data xml-uploader xml-to-spreadsheet:upload /var/www/html/data/file.xml owner.email@gmail.com

where

    /absolute/path/to/the/directory/with/file/to/upload - is the absolute path to the directory with the file we want to upload
